<?php

namespace Planet17\MessageQueueLibraryPipelineHandlers\Interfaces\Handlers;

use Planet17\MessageQueueLibrary\Interfaces\Handlers\HandlerInterface;
use Planet17\MessageQueueLibrary\Interfaces\Messages\MessageInterface;

/**
 * Interface HandlerPipelinedInterface
 *
 * @package Planet17\MessageQueueLibraryPipelineHandlers\Interfaces\Handlers
 */
interface HandlerPipelinedInterface extends HandlerInterface
{
    /**
     * Method must implement handle of message
     *
     * @param mixed|string|MessageInterface $payload
     * @param string|null $messageClass
     */
    public function next($payload, string $messageClass = null): void;
}
