<?php

namespace Planet17\MessageQueueLibraryPipelineHandlers\Handlers;

use Planet17\MessageQueueLibrary\Handlers\BaseHandler;
use Planet17\MessageQueueLibrary\Interfaces\Messages\MessageInterface;
use Planet17\MessageQueueLibraryPipelineHandlers\Exceptions\Handlers\MessageClassNotDefinedException;
use Planet17\MessageQueueLibraryPipelineHandlers\Interfaces\Handlers\HandlerPipelinedInterface;

/**
 * Class PipelinedHandler
 *
 * @package Planet17\MessageQueueLibraryPipelineHandlers\Handlers
 */
class PipelinedHandler extends BaseHandler implements HandlerPipelinedInterface
{
    /** @var null|string Reference to MessageInterface class name */
    protected $nextMessageClass = null;

    /** @var null|mixed Payload will be provided to next pipe. */
    protected $payloadNext = null;

    /** @inheritdoc */
    public function next($payload, ?string $messageClass = null): void
    {
        $this
            ->setPayloadNext($payload)
            ->makeMessage($messageClass)
            ->dispatch();

        $this->resetPayloadNext();
    }

    /**
     * Getter method.
     *
     * @return mixed|null
     */
    public function getPayloadNext()
    {
        return $this->payloadNext;
    }

    /**
     * Setter method.
     *
     * @param mixed|null $payloadNext
     *
     * @return PipelinedHandler
     */
    public function setPayloadNext($payloadNext = null): PipelinedHandler
    {
        $this->payloadNext = $payloadNext;

        return $this;
    }

    /**
     * Reset method.
     *
     * @return PipelinedHandler
     */
    public function resetPayloadNext(): PipelinedHandler
    {
        return $this->setPayloadNext(null);
    }

    /**
     * Method make message for dispatching it for next pipe.
     *
     * @param string|null $messageClass
     *
     * @return MessageInterface
     */
    final protected function makeMessage(?string $messageClass = null): MessageInterface
    {
        $messageClass = $this->resolveMessageClass($messageClass);

        return new $messageClass($this->getPayloadNext());
    }

    /**
     * Method extract resolving what class will be use for making message.
     *
     * @param string|null $messageClass
     *
     * @return string
     *
     * @throws MessageClassNotDefinedException
     */
    final protected function resolveMessageClass(?string $messageClass = null): string
    {
        if ($messageClass) {
            return $messageClass;
        }

        if (!$this->nextMessageClass) {
            throw new MessageClassNotDefinedException;
        }

        return $this->nextMessageClass;
    }
}
