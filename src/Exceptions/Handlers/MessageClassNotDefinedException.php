<?php

namespace Planet17\MessageQueueLibraryPipelineHandlers\Exceptions\Handlers;

use RuntimeException;
use Throwable;

/**
 * Class RouteClassNotDefinedException
 *
 * @package Planet17\MessageQueueLibrary\Exceptions\Handlers
 */
class MessageClassNotDefinedException extends RuntimeException
{
    /** @const DEFAULT_MESSAGE */
    protected const DEFAULT_MESSAGE = 'Message class for pipeline must has been set up or provided';

    /**
     * RouteClassNotDefinedException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = self::DEFAULT_MESSAGE, $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
